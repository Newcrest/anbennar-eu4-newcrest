namespace = deepwoods

##End of Verdant Pact
country_event = {
	id = deepwoods.1
	title = deepwoods.1.t
	desc = deepwoods.1.d
	picture = ARTWORK_eventPicture
	
	fire_only_once = yes
	hidden = yes
	
	mean_time_to_happen = {
		years = 1
	}
	
	trigger = {
		NOT = { has_global_flag = end_verdant_pact }
		calc_true_if = {
			all_province = {
				superregion = deepwoods_superregion
				owner = { primary_culture = wood_elf }
			}
			amount = 45
		}
	}
	
	option = {
        name = deepwoods.1.a
        ai_chance = { factor = 100 }
		every_country = {
			limit = { has_country_modifier = deepwoods_verdant_pact }
			set_country_flag = verdant_pact_flag
			country_event = { id = deepwoods.2 }
		}
		set_global_flag = end_verdant_pact
	}
}
#### End of the Verdant Pact
country_event = {
	id = deepwoods.2
	title = deepwoods.2.t
	desc = deepwoods.2.d
	picture = ARTWORK_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_country_modifier = deepwoods_verdant_pact
	}
	
	#They're coming from the Old Hold !
	option = {
        name = deepwoods.2.a
        ai_chance = { factor = 100 }
		every_country = {
			limit = { has_country_flag = verdant_pact_flag }
			remove_historical_friend = ROOT
		}
		remove_country_modifier = deepwoods_verdant_pact
		swap_non_generic_missions = yes
	}
}
##################################
########Liberate elven country
#################################
#I26
country_event = {
	id = deepwoods.3
	title = deepwoods.3.t
	desc = deepwoods.3.d
	picture = ARTWORK_eventPicture
	
	fire_only_once = yes
	
	mean_time_to_happen = {
		months = 1
	}
	
	trigger = {
		has_country_modifier = deepwoods_verdant_pact
		is_subject = no
		NOT = { exists = I26 }
		any_owned_province = {
			OR = {
				province_id = 3004
				province_id = 3005
				province_id = 3006
				province_id = 3007
				province_id = 3008
			}
		}
	}
	
	#Liberate them
	option = {
        name = deepwoods.3.a
        ai_chance = { factor = 100 }
		every_province = {
			limit = {
				OR = {
					province_id = 3004
					province_id = 3005
					province_id = 3006
					province_id = 3007
					province_id = 3008
				}
			}
			remove_core = ROOT
			add_core = I26
		}
		release = I26
		vassalize = I26
		I26 = {
			add_country_modifier = {
				name = deepwoods_verdant_pact
				duration = -1
			}
			every_country = {
				limit = { has_country_modifier = deepwoods_verdant_pact }
				add_historical_friend = ROOT
				ROOT = { add_historical_friend = PREV }
			}
			every_owned_province = {
				change_culture = wood_elf
				change_religion = fey_court
			}
			add_stability = 1
		}
	}
	
	#Keep the territory
	option = {
        name = deepwoods.3.b
        ai_chance = { factor = 0 }
		add_stability = -1
		add_prestige = -10
		remove_country_modifier = deepwoods_verdant_pact
	}
}

#I27
country_event = {
	id = deepwoods.4
	title = deepwoods.4.t
	desc = deepwoods.4.d
	picture = ARTWORK_eventPicture
	
	fire_only_once = yes
	
	mean_time_to_happen = {
		months = 1
	}
	
	trigger = {
		has_country_modifier = deepwoods_verdant_pact
		is_subject = no
		NOT = { exists = I27 }
		any_owned_province = {
			OR = {
				province_id = 3018
				province_id = 3019
				province_id = 3020
				province_id = 3021
				province_id = 3022
			}
		}
	}
	
	#Liberate them
	option = {
        name = deepwoods.4.a
        ai_chance = { factor = 100 }
		every_province = {
			limit = {
				OR = {
					province_id = 3018
					province_id = 3019
					province_id = 3020
					province_id = 3021
					province_id = 3022
				}
			}
			remove_core = ROOT
			add_core = I27
		}
		release = I27
		vassalize = I27
		I27 = {
			add_country_modifier = {
				name = deepwoods_verdant_pact
				duration = -1
			}
			every_country = {
				limit = { has_country_modifier = deepwoods_verdant_pact }
				add_historical_friend = ROOT
				ROOT = { add_historical_friend = PREV }
			}
			every_owned_province = {
				change_culture = wood_elf
				change_religion = fey_court
			}
			add_stability = 1
		}
	}
	
	#Keep the territory
	option = {
        name = deepwoods.4.b
        ai_chance = { factor = 0 }
		add_stability = -1
		add_prestige = -10
		remove_country_modifier = deepwoods_verdant_pact
	}
}

#I28
country_event = {
	id = deepwoods.5
	title = deepwoods.5.t
	desc = deepwoods.5.d
	picture = ARTWORK_eventPicture
	
	fire_only_once = yes
	
	mean_time_to_happen = {
		months = 1
	}
	
	trigger = {
		has_country_modifier = deepwoods_verdant_pact
		is_subject = no
		NOT = { exists = I28 }
		any_owned_province = {
			OR = {
				province_id = 3013
				province_id = 3014
				province_id = 3015
				province_id = 3016
				province_id = 3017
			}
		}
	}
	
	#Liberate them
	option = {
        name = deepwoods.5.a
        ai_chance = { factor = 100 }
		every_province = {
			limit = {
				OR = {
					province_id = 3013
					province_id = 3014
					province_id = 3015
					province_id = 3016
					province_id = 3017
				}
			}
			remove_core = ROOT
			add_core = I28
		}
		release = I28
		vassalize = I28
		I28 = {
			add_country_modifier = {
				name = deepwoods_verdant_pact
				duration = -1
			}
			every_country = {
				limit = { has_country_modifier = deepwoods_verdant_pact }
				add_historical_friend = ROOT
				ROOT = { add_historical_friend = PREV }
			}
			every_owned_province = {
				change_culture = wood_elf
				change_religion = fey_court
			}
			add_stability = 1
		}
	}
	
	#Keep the territory
	option = {
        name = deepwoods.5.b
        ai_chance = { factor = 0 }
		add_stability = -1
		add_prestige = -10
		remove_country_modifier = deepwoods_verdant_pact
	}
}

#I29
country_event = {
	id = deepwoods.6
	title = deepwoods.6.t
	desc = deepwoods.6.d
	picture = ARTWORK_eventPicture
	
	fire_only_once = yes
	
	mean_time_to_happen = {
		months = 1
	}
	
	trigger = {
		has_country_modifier = deepwoods_verdant_pact
		is_subject = no
		NOT = { exists = I29 }
		any_owned_province = {
			OR = {
				province_id = 3009
				province_id = 3010
				province_id = 3011
				province_id = 3012
				province_id = 3067
			}
		}
	}
	
	#Liberate them
	option = {
        name = deepwoods.6.a
        ai_chance = { factor = 100 }
		every_province = {
			limit = {
				OR = {
					province_id = 3009
					province_id = 3010
					province_id = 3011
					province_id = 3012
					province_id = 3067
				}
			}
			remove_core = ROOT
			add_core = I29
		}
		release = I29
		vassalize = I29
		I29 = {
			add_country_modifier = {
				name = deepwoods_verdant_pact
				duration = -1
			}
			every_country = {
				limit = { has_country_modifier = deepwoods_verdant_pact }
				add_historical_friend = ROOT
				ROOT = { add_historical_friend = PREV }
			}
			every_owned_province = {
				change_culture = wood_elf
				change_religion = fey_court
			}
			add_stability = 1
		}
	}
	
	#Keep the territory
	option = {
        name = deepwoods.6.b
        ai_chance = { factor = 0 }
		add_stability = -1
		add_prestige = -10
		remove_country_modifier = deepwoods_verdant_pact
	}
}

#I30
country_event = {
	id = deepwoods.7
	title = deepwoods.7.t
	desc = deepwoods.7.d
	picture = ARTWORK_eventPicture
	
	fire_only_once = yes
	
	mean_time_to_happen = {
		months = 1
	}
	
	trigger = {
		has_country_modifier = deepwoods_verdant_pact
		is_subject = no
		NOT = { exists = I30 }
		any_owned_province = {
			OR = {
				province_id = 3023
				province_id = 3024
				province_id = 3025
				province_id = 3026
			}
		}
	}
	
	#Liberate them
	option = {
        name = deepwoods.7.a
        ai_chance = { factor = 100 }
		every_province = {
			limit = {
				OR = {
					province_id = 3023
					province_id = 3024
					province_id = 3025
					province_id = 3026
				}
			}
			remove_core = ROOT
			add_core = I30
		}
		release = I30
		vassalize = I30
		I30 = {
			add_country_modifier = {
				name = deepwoods_verdant_pact
				duration = -1
			}
			every_country = {
				limit = { has_country_modifier = deepwoods_verdant_pact }
				add_historical_friend = ROOT
				ROOT = { add_historical_friend = PREV }
			}
			every_owned_province = {
				change_culture = wood_elf
				change_religion = fey_court
			}
			add_stability = 1
		}
	}
	
	#Keep the territory
	option = {
        name = deepwoods.7.b
        ai_chance = { factor = 0 }
		add_stability = -1
		add_prestige = -10
		remove_country_modifier = deepwoods_verdant_pact
	}
}

#I31
country_event = {
	id = deepwoods.8
	title = deepwoods.8.t
	desc = deepwoods.8.d
	picture = ARTWORK_eventPicture
	
	fire_only_once = yes
	
	mean_time_to_happen = {
		months = 1
	}
	
	trigger = {
		has_country_modifier = deepwoods_verdant_pact
		is_subject = no
		NOT = { exists = I31 }
		any_owned_province = {
			OR = {
				province_id = 3027
				province_id = 3028
				province_id = 3029
				province_id = 3030
				province_id = 3031
			}
		}
	}
	
	#Liberate them
	option = {
        name = deepwoods.8.a
        ai_chance = { factor = 100 }
		every_province = {
			limit = {
				OR = {
					province_id = 3027
					province_id = 3028
					province_id = 3029
					province_id = 3030
					province_id = 3031
				}
			}
			remove_core = ROOT
			add_core = I31
		}
		release = I31
		vassalize = I31
		I31 = {
			add_country_modifier = {
				name = deepwoods_verdant_pact
				duration = -1
			}
			every_country = {
				limit = { has_country_modifier = deepwoods_verdant_pact }
				add_historical_friend = ROOT
				ROOT = { add_historical_friend = PREV }
			}
			every_owned_province = {
				change_culture = wood_elf
				change_religion = fey_court
			}
			add_stability = 1
		}
	}
	
	#Keep the territory
	option = {
        name = deepwoods.8.b
        ai_chance = { factor = 0 }
		add_stability = -1
		add_prestige = -10
		remove_country_modifier = deepwoods_verdant_pact
	}
}

#I32
country_event = {
	id = deepwoods.9
	title = deepwoods.9.t
	desc = deepwoods.9.d
	picture = ARTWORK_eventPicture
	
	fire_only_once = yes
	
	mean_time_to_happen = {
		months = 1
	}
	
	trigger = {
		has_country_modifier = deepwoods_verdant_pact
		is_subject = no
		NOT = { exists = I32 }
		any_owned_province = {
			OR = {
				province_id = 3039
				province_id = 3040
				province_id = 3041
				province_id = 3042
			}
		}
	}
	
	#Liberate them
	option = {
        name = deepwoods.9.a
        ai_chance = { factor = 100 }
		every_province = {
			limit = {
				OR = {
					province_id = 3039
					province_id = 3040
					province_id = 3041
					province_id = 3042
				}
			}
			remove_core = ROOT
			add_core = I32
		}
		release = I32
		vassalize = I32
		I32 = {
			add_country_modifier = {
				name = deepwoods_verdant_pact
				duration = -1
			}
			every_country = {
				limit = { has_country_modifier = deepwoods_verdant_pact }
				add_historical_friend = ROOT
				ROOT = { add_historical_friend = PREV }
			}
			every_owned_province = {
				change_culture = wood_elf
				change_religion = fey_court
			}
			add_stability = 1
		}
	}
	
	#Keep the territory
	option = {
        name = deepwoods.9.b
        ai_chance = { factor = 0 }
		add_stability = -1
		add_prestige = -10
		remove_country_modifier = deepwoods_verdant_pact
	}
}

#I33
country_event = {
	id = deepwoods.10
	title = deepwoods.10.t
	desc = deepwoods.10.d
	picture = ARTWORK_eventPicture
	
	fire_only_once = yes
	
	mean_time_to_happen = {
		months = 1
	}
	
	trigger = {
		has_country_modifier = deepwoods_verdant_pact
		is_subject = no
		NOT = { exists = I33 }
		any_owned_province = {
			OR = {
				province_id = 3032
				province_id = 3033
				province_id = 3034
				province_id = 3035
				province_id = 3036
				province_id = 3037
				province_id = 3038
			}
		}
	}
	
	#Liberate them
	option = {
        name = deepwoods.10.a
        ai_chance = { factor = 100 }
		every_province = {
			limit = {
				OR = {
					province_id = 3032
					province_id = 3033
					province_id = 3034
					province_id = 3035
					province_id = 3036
					province_id = 3037
					province_id = 3038
				}
			}
			remove_core = ROOT
			add_core = I33
		}
		release = I33
		vassalize = I33
		I33 = {
			add_country_modifier = {
				name = deepwoods_verdant_pact
				duration = -1
			}
			every_country = {
				limit = { has_country_modifier = deepwoods_verdant_pact }
				add_historical_friend = ROOT
				ROOT = { add_historical_friend = PREV }
			}
			every_owned_province = {
				change_culture = wood_elf
				change_religion = fey_court
			}
			add_stability = 1
		}
	}
	
	#Keep the territory
	option = {
        name = deepwoods.10.b
        ai_chance = { factor = 0 }
		add_stability = -1
		add_prestige = -10
		remove_country_modifier = deepwoods_verdant_pact
	}
}

#I34
country_event = {
	id = deepwoods.11
	title = deepwoods.11.t
	desc = deepwoods.11.d
	picture = ARTWORK_eventPicture
	
	fire_only_once = yes
	
	mean_time_to_happen = {
		months = 1
	}
	
	trigger = {
		has_country_modifier = deepwoods_verdant_pact
		is_subject = no
		NOT = { exists = I34 }
		any_owned_province = {
			OR = {
				province_id = 3043
				province_id = 3044
				province_id = 3045
				province_id = 3046
				province_id = 3047
			}
		}
	}
	
	#Liberate them
	option = {
        name = deepwoods.11.a
        ai_chance = { factor = 100 }
		every_province = {
			limit = {
				OR = {
					province_id = 3043
					province_id = 3044
					province_id = 3045
					province_id = 3046
					province_id = 3047
				}
			}
			remove_core = ROOT
			add_core = I34
		}
		release = I34
		vassalize = I34
		I34 = {
			add_country_modifier = {
				name = deepwoods_verdant_pact
				duration = -1
			}
			every_country = {
				limit = { has_country_modifier = deepwoods_verdant_pact }
				add_historical_friend = ROOT
				ROOT = { add_historical_friend = PREV }
			}
			every_owned_province = {
				change_culture = wood_elf
				change_religion = fey_court
			}
			add_stability = 1
		}
	}
	
	#Keep the territory
	option = {
        name = deepwoods.11.b
        ai_chance = { factor = 0 }
		add_stability = -1
		add_prestige = -10
		remove_country_modifier = deepwoods_verdant_pact
	}
}

#Give Back Core
country_event = {
	id = deepwoods.12
	title = deepwoods.12.t
	desc = deepwoods.12.d
	picture = ARTWORK_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_country_modifier = deepwoods_verdant_pact
		any_owned_province = {
			NOT = { is_core = ROOT }
			any_core_country = {
				has_country_modifier = deepwoods_verdant_pact
				primary_culture = wood_elf
				exists = yes
			}
		}
	}
	
	#Give the territory back
	option = {
        name = deepwoods.12.a
        ai_chance = { factor = 100 }
		add_stability = 1
		every_owned_province = {
			limit = { NOT = { is_core = ROOT } }
			random_core_country = {
				limit = { 
					primary_culture = wood_elf 
					exists = yes 
					has_country_modifier = deepwoods_verdant_pact
				}
				add_trust = {
					who = ROOT
					value = 3
				}
				add_prestige = 3
				PREV = { cede_province = PREV }
			}
		}
	}
	
	#Keep the territory
	option = {
        name = deepwoods.12.b
        ai_chance = { factor = 0 }
		add_stability = -1
		add_prestige = -5
	}
}

#The Verdant Union
country_event = {
	id = deepwoods.13
	title = deepwoods.13.t
	desc = deepwoods.13.d
	picture = ARTWORK_eventPicture
	
	fire_only_once = yes
	
	mean_time_to_happen = {
		years = 10
		
		modifier = {
			factor = 0.5
			
			ai = no
		}
	}
	
	trigger = {
		primary_culture = wood_elf
		capital_scope = { superregion = deepwoods_superregion }
		total_development = 60
		is_at_war = no
		is_subject = no
		OR = {
			AND = {
				NOT = { is_female = yes }
				any_known_country = {
					primary_culture = wood_elf
					capital_scope = { superregion = deepwoods_superregion }
					is_female = yes
					ai = yes
					total_development = 60
					is_subject = no
				}
			}
			AND = {
				is_female = yes
				any_known_country = {
					primary_culture = wood_elf
					capital_scope = { superregion = deepwoods_superregion }
					NOT = { is_female = yes }
					ai = yes
					total_development = 60
					is_subject = no
				}
			}
		}
	}
	
	#Seal the union
	option = {
        name = deepwoods.13.a
        ai_chance = { factor = 100 }
		add_stability = 1
		add_prestige = 15
		if = {
			limit = { NOT = { is_female = yes } }
			random_known_country = {
				limit = {
					primary_culture = wood_elf
					capital_scope = { superregion = deepwoods_superregion }
					is_female = yes
					ai = yes
					total_development = 60
					is_subject = no
				}
				ROOT = { create_union = PREV }
			}
		}
		else = {
			random_known_country = {
				limit = {
					primary_culture = wood_elf
					capital_scope = { superregion = deepwoods_superregion }
					NOT = { is_female = yes }
					ai = yes
					total_development = 60
					is_subject = no
				}
				ROOT = { create_union = PREV }
			}
		}
	}
}

###Emerald orc spawning
country_event = {
	id = deepwoods.14
	title = deepwoods.14.t
	desc = deepwoods.14.d
	picture = ARTWORK_eventPicture

	fire_only_once = yes
	
	trigger = {
		capital_scope = {
			superregion = deepwoods_superregion
		}
		primary_culture = green_orc
		total_development = 100
		is_female = no
		has_regency = no
	}
	
	mean_time_to_happen = {
		years = 20
		
		modifier = {
			factor = 0.9
			mil = 4
		}
		modifier = {
			factor = 0.9
			mil = 5
		}
		modifier = {
			factor = 0.9
			mil = 6
		}
		modifier = {
			factor = 0.5
			ai = no
		}
	}

	# I accept your offer...
	option = {
		name = deepwoods.14.a
		ai_chance = {
			factor = 75
		}
		add_stability = -1
		add_prestige = -10
		add_yearly_manpower = 2
		custom_tooltip = emerald_orc_appearance
		hidden_effect = {
			change_tag = I46
			swap_non_generic_missions = yes
			if = {
				limit = { NOT = { government = tribe } }
				change_government = tribe
			}
			add_government_reform = emerald_horde
			change_mil = 6
			if = {
				limit = { ruler_has_max_personalities = yes }
				clear_scripted_personalities = yes
				add_ruler_personality = mage_personality
			}
			add_ruler_modifier = {
				name = emerald_orc_fey_blessing
				duration = -1
			}
			set_ruler_culture = emerald_orc
			set_ruler_religion = ashentree_pact
			change_religion = ashentree_pact
			
			change_primary_culture = emerald_orc
			every_owned_province = {
				if = {
					limit = { culture = green_orc }
					change_culture = emerald_orc
					change_religion = ashentree_pact
				}
			}
			
			set_global_flag = emerald_orcs_are_created
			country_event = { id = ideagroups.1 } #Swap Ideas
		}
	}
	# We will never be fey servants!
	option = {
		name = deepwoods.14.b
		ai_chance = {
			factor = 25
			modifier = {
				factor = 0
				has_global_flag = end_verdant_pact
			}
		}
		add_stability = 1
		add_prestige = 5
	}
}

#The fall of house X
country_event = {
	id = deepwoods.15
	title = deepwoods.15.t
	desc = deepwoods.15.d
	picture = ARTWORK_eventPicture
	
	fire_only_once = yes
	
	mean_time_to_happen = {
		years = 5
	}
	
	trigger = {
		primary_culture = wood_elf
		capital_scope = { superregion = deepwoods_superregion }
		has_global_flag = end_verdant_pact
		is_year = 1500
		total_development = 60
		is_at_war = no
		is_subject = no
		ai = yes
		any_neighbor_country = {
			primary_culture = wood_elf
			capital_scope = { superregion = deepwoods_superregion }
			is_subject = no
			NOT = { total_development = 150 }
		}
	}
	
	#Assume control
	option = {
        name = deepwoods.15.a
        ai_chance = { factor = 100 }
		random_neighbor_country = {
			limit = {
				primary_culture = wood_elf
				capital_scope = { superregion = deepwoods_superregion }
				is_subject = no
				NOT = { total_development = 150 }
			}
			every_owned_province = {
				cede_province = ROOT
				add_core = ROOT
			}
		}
	}
}

#############Debug shit
#discover shit
country_event = {
	id = deepwoods.100
	title = deepwoods.100.t
	desc = deepwoods.100.d
	picture = ARTWORK_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	hidden = yes
	
	trigger = {
		NOT = { has_global_flag = deepwoods_initialized }
	}
	
	option = {
        name = deepwoods.2.a
        ai_chance = { factor = 100 }
		country_event = { id = deepwoods.101 days = 1 }
	}
}

country_event = {
	id = deepwoods.101
	title = deepwoods.100.t
	desc = deepwoods.100.d
	picture = ARTWORK_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	hidden = yes
	
	trigger = {
		NOT = { has_global_flag = deepwoods_initialized }
	}
	
	option = {
        name = deepwoods.2.a
        ai_chance = { factor = 100 }
		every_country = {
			limit = {
				capital_scope = { superregion = deepwoods_superregion }
			}
			every_province = { undiscover_country = PREV }
			deepwoods_superregion = {
				discover_country = PREV
			}
			1782 = { discover_country = PREV } #Deepwoods wasteland
			1261 = { discover_country = PREV } #Deepwoods Lake
		}
		set_global_flag = deepwoods_initialized
	}
}

