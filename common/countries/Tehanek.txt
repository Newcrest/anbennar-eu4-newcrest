#Country Name: Please see filename.

graphical_culture = northamericagfx

color = { 156  67  243 }

revolutionary_colors = { 156  67  243 }

historical_idea_groups = {
	economic_ideas
	offensive_ideas
	influence_ideas
	defensive_ideas	
	administrative_ideas	
	trade_ideas
	quality_ideas
	innovativeness_ideas
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	native_indian_horsemen
	huron_arquebusier
	commanche_swarm
	native_indian_mountain_warfare
	american_western_franchise_warfare
}

monarch_names = {

	#Generic Aelantiri Names
	"Adra'hai #0" = 10
	"Adra'haio #0" = 10
	"Alarenu #0" = 10
	"Ald'ar #0" = 10
	"Ald'aro #0" = 10
	"Andrel'on #0" = 10
	"Apel #0" = 10
	"Aran #0" = 10
	"Ardo #0" = 10
	"Ardo'i #0" = 10
	"Arna'do #0" = 10
	"Arto'i #0" = 10
	"Artoi #0" = 10
	"Bake'renne #0" = 10
	"Benedo #0" = 10
	"Camno #0" = 10
	"Camno'ari #0" = 10
	"Camno'arin #0" = 10
	"Caro #0" = 10
	"Caro'do #0" = 10
	"Cela'dole #0" = 10
	"Dorasto'entu #0" = 10
	"Doren'do #0" = 10
	"Ebran #0" = 10
	"Ebran'do #0" = 10
	"Eebo #0" = 10
	"Ele'il #0" = 10
	"Elr #0" = 10
	"Elro #0" = 10
	"Elthe'das #0" = 10
	"Elthe'dor #0" = 10
	"Elthe'thar #0" = 10
	"Err'ir #0" = 10
	"Err'lasa #0" = 10
	"Evn'do #0" = 10
	"Evn'do #0" = 10
	"Evn'enn #0" = 10
	"Fin'o #0" = 10
	"Galendo #0" = 10
	"Galenel #0" = 10
	"Geln #0" = 10
	"Gelr #0" = 10
	"Jarre #0" = 10
	"Jarreh #0" = 10
	"Kaland #0" = 10
	"Kalandu #0" = 10
	"Kalin'do #0" = 10
	"Kel'do #0" = 10
	"Kel'doro #0" = 10
	"Monda #0" = 10
	"Nestetu #0" = 10
	"Olo #0" = 10
	"Olo'don #0" = 10
	"Olodo #0" = 10
	"Pele'na #0" = 10
	"Pelo #0" = 10
	"Seron'ao #0" = 10
	"Seron'ar #0" = 10
	"Seron'do #0" = 10
	"Seron'o #0" = 10
	"Talai #0" = 10
	"Talor #0" = 10
	"Teler'osan #0" = 10
	"Teler'osandai #0" = 10
	"Teler'osandar #0" = 10
	"Threthensdo #0" = 10
	"Tiren #0" = 10
	"Tiren'do #0" = 10
	"Tiren'hai #0" = 10
	"Ultar #0" = 10
	"Uron #0" = 10
	"Vanai #0" = 10
	"Vara'male #0" = 10
	"Vara'mall #0" = 10
	"Vara'malleno #0" = 10
	"Vara'mar #0" = 10
	"Varel'do #0" = 10
	"Varn #0" = 10
	
	"Ala'a #0" = -10
	"Alar'a #0" = -10
	"Alera'ar #0" = -10
	"Alra'e #0" = -10
	"Arat'a #0" = -10
	"Arat'e #0" = -10
	"Arat'o #0" = -10
	"Cala'a #0" = -10
	"Cel'ada #0" = -10
	"Cema'denn #0" = -10
	"Deve #0" = -10
	"Deve'ne #0" = -10
	"Eb'den #0" = -10
	"Eestra #0" = -10
	"El'den #0" = -10
	"Ela'a #0" = -10
	"Ere'denn #0" = -10
	"Fel'asa #0" = -10
	"Galenel #0" = -10
	"Imar'el #0" = -10
	"Isere #0" = -10
	"Isere'el #0" = -10
	"Ishe'e #0" = -10
	"Ivran'a #0" = -10
	"Ivran'e #0" = -10
	"Jexa'al #0" = -10
	"Laden'al #0" = -10
	"Ladena #0" = -10
	"Lana'ar #0" = -10
	"Lana'le #0" = -10
	"Lelia'ar #0" = -10
	"Leyan'd #0" = -10
	"Leyan'de #0" = -10
	"Meyal'e #0" = -10
	"Mith'le #0" = -10
	"Nara #0" = -10
	"Nathel'ar #0" = -10
	"Pana #0" = -10
	"Rel'asa #0" = -10
	"Saeran'le #0" = -10
	"Selu'osa #0" = -10
	"Sero'denn #0" = -10
	"Shar'a #0" = -10
	"Sharin'ar #0" = -10
	"Sharin'el #0" = -10
	"Sheva'le #0" = -10
	"Tene'a #0" = -10
	"Tha'ada #0" = -10
	"Var'i #0" = -10
	"Var'ie #0" = -10
	"Veha'i #0" = -10
	"Vehari #0" = -10
	"Zale'ar #0" = -10
	
}

leader_names = {

	#Just random native stuff to be honest
	Awegen Ahaouet
	Canajoharie Canastigaone Canienga Caughnawaga Cahunghage Canowaroghere
	Deseroken Dayoitgao Deonundagae Deyodeshot Deyohnegano Deyonongdadagana
	Gweugwehono Gandasetaigon Ganogeh Gayagaanhe Gewauga Goiogouen Gadoquat Gannentaha
	Hostayuntwa
	Kawauka Kente Kanagaro Kowogoconnughariegugharie Kanadaseagea Kanatakowa
	Neodakheat Nowadaga Nundawaono
	Onekagoncka Onoalagona Oquaga Osquake Onayotekaono Oriska Ossewingo 
	Skannayutenate Saratoga Schaunactada Schoharie
	Teatontaloga Tewanondadon Tionnontoguen Tegasoke Teseroken Tetosweken 
	Yoroonwago
	
	Aisikstukiks
	Apikaiyikis
	Emitahpahksaiyiks
	Motahtosiks
	Puhksinahmahyiks
	Saiyiks
	Siksinokaks
	Tsiniktsistsoyiks
	Ahkaisumiks
	Ahkaipokaks
	Ahkotahshiks
	Anepo
	Apikaiyiks
	Aputosikainah
	Inuhksoyistamiks
	Isissokasimiks
	Istsikaihan
	Mameoya
	Nitikskiks
	Saksinahmahyiks
	Sikahpuniks
	Sikinokaks
	Ahahpitape
	Ahkaiyikokakiniks
	Apikaiyiks
	Esksinaitupiks
	Inuksikahpowaiks
	Inuksiks
	Ipoksimaiks
	Kahmitaiks
	Kiyis
	Kutaiimiks
	Kutaisotsiman
	Miahwahpitsiks
	Miawkinaiyiks
	Mokumiks
	Motahtosiks
	Motwainaiks
	Nitakoskitsipupiks
	Niwayiks
	Nitikskiks
	Nitotsiksistaniks
	Sikokitsimiks
	Sikopoksimaiks
	Sikutsipumaiks
	Susksoyiks
	Tsiniksistsoyiks
	
}

ship_names = {
	Atahensic Ataensic Adekagagwaa
	Gaol Gohone Gah-oh Gendenwitha
	Hahgwehdiyu Hahgwehdaetgan "Ha Wen Neyu"
	Jogah
	Losheka
	Onatha Oki
	Sosondowah
	Tarhuhyiawahku
	Yosheka Ya-o-gah
}	

army_names = {
	"Tehanek Army" "Army of $PROVINCE$"
}

fleet_names = {
	"Tehanek Fleet"
}