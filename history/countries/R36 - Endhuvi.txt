government = monarchy
add_government_reform = great_jamindar_reform
government_rank = 1
primary_culture = west_harimari
add_accepted_culture = rasarhid
religion = high_philosophy
technology_group = tech_harimari
religious_school = unbroken_claw_school
capital = 4492

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Vapyata"
		dynasty = "Rahara"
		birth_date = 1400.11.1
		adm = 2
		dip = 3
		mil = 6
		culture = west_harimari
	}
}