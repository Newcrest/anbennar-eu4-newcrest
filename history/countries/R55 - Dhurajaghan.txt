government = monarchy
add_government_reform = jamindar_reform
government_rank = 1
primary_culture = west_harimari
religion = high_philosophy
technology_group = tech_raheni
religious_school = starry_eye_school
capital = 4408

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Pusyamitra"
		dynasty = "Ganesha"
		birth_date = 1392.6.5
		adm = 5
		dip = 2
		mil = 4
		culture = west_harimari
	}
}