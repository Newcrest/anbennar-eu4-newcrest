government = tribal
add_government_reform = greentide_horde
government_rank = 1
primary_culture = brown_orc
religion = great_dookan
technology_group = tech_orcish
national_focus = MIL
capital = 4622

1430.1.1 = {
	monarch = {
		name = "Brovag"
		dynasty = "Bloodsong"
		birth_date = 1410.10.4
		adm = 3
		dip = 3
		mil = 4
	}
}