government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = arbarani
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 253

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1433.6.16 = {
	monarch = {
		name = "Lain II"
		dynasty = "Derwing"
		birth_date = 1431.5.12
		adm = 2
		dip = 3
		mil = 0
	}
}