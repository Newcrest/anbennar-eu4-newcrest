government = republic
add_government_reform = merchants_reform
government_rank = 1
mercantilism = 25
primary_culture = crownsman
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 915 #Crothan
fixed_capital = 915 # Cannot move capital away from this province & no power cost to move to it

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1422.1.1 = { set_country_flag = lilac_wars_rose_party }

1444.1.1 = {
	monarch = {
		name = "City Council"
		adm = 2
		dip = 2
		mil = 2
		regent = yes
	}
}